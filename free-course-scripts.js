window.onscroll = function() { stickHeader() };
let stickyCTA = document.querySelector('#free-course--sticky-cta');
let stickyOffset = stickyCTA.offsetTop;
function stickHeader() {
if (window.pageYOffset > stickyOffset) {
    stickyCTA.classList.add('sticky');
} else {
    stickyCTA.classList.remove('sticky');
}
}
function makeCourseLessons() {
  let courseLessonsWidget = document.querySelector('ul.course-progress-lessons');
  let courseLessons = courseLessonsWidget.getAttribute('data-lessons');
  let courseLessonsArr = courseLessons.split(',');
  courseLessonsArr.forEach((lesson) => { courseLessonsWidget.innerHTML += '<li class="course-progress-lesson not-completed" title="Ready for more? Enroll below &darr;">' + lesson + '</li>'; });
}